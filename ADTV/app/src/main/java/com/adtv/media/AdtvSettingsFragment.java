package com.adtv.media;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Георгий on 11.03.2015.
 */
public class AdtvSettingsFragment extends Fragment {
    private View rootView;
    private Button mButtonLogout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings_adtv, container, false);
        Button mButtonBack = (Button) rootView.findViewById(R.id.fragment_settings_button_back);
        mButtonLogout = (Button) rootView.findViewById(R.id.settings_logout);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        mButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AdtvMainActivity) getActivity()).logoutUser();
                ((AdtvMainActivity) getActivity()).restartApp();
            }
        });
        return rootView;
    }
}
