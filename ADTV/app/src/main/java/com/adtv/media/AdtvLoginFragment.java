package com.adtv.media;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adtv.media.async.AnswerAsyncTask;
import com.adtv.media.async.AnswerAsyncTaskInerface;
import com.adtv.media.async.RatingAsyncTask;
import com.adtv.media.async.RatingAsyncTaskInteface;
import com.adtv.media.async.RegisterAsyncTask;
import com.adtv.media.async.RegisterAsyncTaskInterface;
import com.adtv.media.dto.Address;
import com.adtv.media.dto.City;
import com.adtv.media.dto.Gender;
import com.adtv.media.dto.Registration;
import com.adtv.media.other.Globals;
import com.adtv.media.other.NetworkUtils;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Георгий on 15.03.2015.
 */
public class AdtvLoginFragment extends Fragment implements OnItemSelectedListener {

    private static final Calendar DEFAULT_BIRTHDAY_DATE = new GregorianCalendar(1970, 0, 1);
    private static final String TERMS_URL = "http://uploads.adtv.media/app/trivia_app_rules_and_terms.doc";

    boolean isEnterBirth = false;
    EditText userName;
    EditText userSurname;
    EditText userPhone;
    EditText userEmail;
    Spinner userGender;
    Spinner userCity;
    Spinner userAddress;
    Button mButtonLogin;
    EditText userBirth;
    ScrollView scrollView;
    TextView termsTextView;
    CheckBox acceptTermsCheckBox;

    List<Address> addressList = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_adtv, container, false);

        Gender[] genders = new Gender[3];
        genders[0] = new Gender(null, getResources().getString(R.string.select_gender));
        genders[1] = new Gender(1, getResources().getString(R.string.men));
        genders[2] = new Gender(2, getResources().getString(R.string.woman));

        City[] cities = new City[Globals.cityList.size() + 1];
        cities[0] = new City(null, getResources().getString(R.string.select_city));
        for (int i = 0; i < Globals.cityList.size(); i++) {
            cities[i + 1] = Globals.cityList.get(i);
        }

        ArrayAdapter adapterCity = new ArrayAdapter(this.getActivity(), R.layout.spinner_item, cities);
        ArrayAdapter adapterSex = new ArrayAdapter(this.getActivity(), R.layout.spinner_item, genders);

        userName = (EditText) view.findViewById(R.id.user_name);
        userSurname = (EditText) view.findViewById(R.id.user_surname);
        userPhone = (EditText) view.findViewById(R.id.user_phone);
        userEmail = (EditText) view.findViewById(R.id.user_email);
        userGender = (Spinner) view.findViewById(R.id.user_gender);
        userCity = (Spinner) view.findViewById(R.id.user_city);
        userAddress = (Spinner) view.findViewById(R.id.user_address);
        userBirth = (EditText) view.findViewById(R.id.user_birth);
        mButtonLogin = (Button) view.findViewById(R.id.user_button);
        scrollView = (ScrollView) view.findViewById(R.id.login_scroll);
        acceptTermsCheckBox = (CheckBox) view.findViewById(R.id.accept_terms_checkbox);

        userBirth.setText(getResources().getString(R.string.select_birthday));

        termsTextView = (TextView) view.findViewById(R.id.terms_text_view);
        termsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(TERMS_URL));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
        userGender.setAdapter(adapterSex);
        userCity.setAdapter(adapterCity);
        userGender.setFocusable(true);
        userCity.setFocusable(true);
        userGender.setFocusableInTouchMode(true);
        userCity.setFocusableInTouchMode(true);
        userGender.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    userGender.performClick();
                }
            }
        });

        userCity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    userCity.performClick();
                }
            }
        });
        userAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    userAddress.performClick();
                }
            }
        });
        userGender.setOnItemSelectedListener(this);
        userCity.setOnItemSelectedListener(this);
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendForm();
            }
        });
        userBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        if (!new NetworkUtils(getActivity().getApplicationContext()).isConnectionAvailable()) {
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void sendForm() {
        if (!new NetworkUtils(getActivity().getApplicationContext()).isConnectionAvailable()) {
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!hasError()) {
            RegisterAsyncTask registerAsyncTask = new RegisterAsyncTask(new RegisterAsyncTaskInterface() {
                @Override
                public void processFinish(Registration registration) {
                    if (registration.getSuccess() == null) {
                        progressDialog.dismiss();
                        Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.email_registered), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    } else {
                        ((AdtvMainActivity) getActivity()).registerUser(registration.getUser_id(), registration.getToken());
                        closeFragment();
                    }

                }
            },
                    userName.getText().toString(),
                    userSurname.getText().toString(),
                    ((Address) userAddress.getSelectedItem()).getId(),
                    ((Gender) userGender.getSelectedItem()).getId(),
                    userBirth.getText().toString(),
                    userEmail.getText().toString(),
                    userPhone.getText().toString()
            );
            progressDialog = ProgressDialog.show(getActivity(), getResources().getString(R.string.loading),
                    getResources().getString(R.string.pleas_wait), false, false);
            progressDialog.show();

            registerAsyncTask.execute();
        }
    }

    private boolean hasError() {
        boolean result = false;
        if (userName.getText().toString().trim().equalsIgnoreCase("")) {
            result = true;
        }
        if (userSurname.getText().toString().trim().equalsIgnoreCase("")) {
            result = true;
        }
        if (userPhone.getText().toString().trim().equalsIgnoreCase("")) {
            result = true;
        }
        if (userBirth.getText().toString().trim().equalsIgnoreCase("")) {
            result = true;
        }
        if (userBirth.getText().toString().equals(getResources().getString(R.string.select_birthday).toLowerCase())) {
            result = true;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            formatter.parse(userBirth.getText().toString());
        } catch (ParseException e) {
            result = true;
        }
        if (userCity.getSelectedItem() == null || ((City) userCity.getSelectedItem()).getId() == null) {
            result = true;
        }

        if (userAddress.getSelectedItem() == null || ((Address) userAddress.getSelectedItem()).getId() == null) {
            result = true;
        }

        if (userGender.getSelectedItem() == null || ((Gender) userGender.getSelectedItem()).getId() == null) {
            result = true;
        }

        if (!acceptTermsCheckBox.isChecked()) {
            result = true;
        }

        if (result) {
            Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.required), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return true;
        }
        if (!isValidEmail(userEmail.getText().toString())) {
            Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.email_incorrect), Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return true;
        }
        return result;
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.user_city) {
            City city = (City) spinner.getSelectedItem();
            if (city.getId() != null) {
                getAddressByCity();
            }
        }
    }

    private void fillAddressSpinner(List<Address> al) {
        ArrayAdapter adapterAddress = new ArrayAdapter(this.getActivity(), R.layout.spinner_item, al);
        userAddress.setAdapter(adapterAddress);
        userAddress.requestFocus();
    }

    private void getAddressByCity() {
        new GetAddressAsyncTask().execute();
    }

    public class GetAddressAsyncTask extends AsyncTask<Void, Void, String> {
        private final String urlAddresses = URLConstants.GET_ADDRESSES;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

            progressDialog = ProgressDialog.show(getActivity(), getResources().getString(R.string.loading),
                    getResources().getString(R.string.pleas_wait), false, false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            JSONObject json = new JSONObject();
            String request = "";
            try {
                json.put("city_id", ((City) userCity.getSelectedItem()).getId());
                System.out.println(request);
                request = UrlConnectionUtils.postData(urlAddresses, json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return request;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("RESPONSE: " + s);
            try {
                String json;
                JSONObject jsonObject = new JSONObject(s);
                json = jsonObject.getString("response");

                Type type = new TypeToken<List<Address>>() {
                }.getType();
                addressList = new Gson().fromJson(json, type);
                for (int i = 0; i < addressList.size(); i++) {
                    String str = addressList.get(i).getBuilding_name();
                    String strEnd = "";
                    char[] chars = str.toCharArray();
                    for (int j = 0; j < chars.length; j++) {
                        if (j == 0 && chars[j] == ',') {

                        } else if (j == 1 && chars[j] == '\'') {

                        } else if (j == chars.length - 1 && chars[j] == ',') {

                        } else if (j == chars.length - 2 && chars[j] == '\'') {

                        } else {
                            strEnd = strEnd + chars[j];
                        }

                    }
                    addressList.get(i).setBuilding_name(strEnd);
                }
                // After completing http call
                progressDialog.dismiss();
                scrollView.fullScroll(View.FOCUS_DOWN);
                fillAddressSpinner(addressList);

            } catch (JSONException e) {
                Log.d("myLog", "GetCitiesAsyncTask error is: " + e.getMessage());
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        System.out.println("NO SELECTED");
    }

    /**
     * Date Picker
     */

    private void showDatePicker(final Date date) {
        int[] yearMonthDay = ymdTripleFor(date);
        final Calendar calendar = Calendar.getInstance();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        builder.setView(LayoutInflater.from(getActivity()).inflate(R.layout.date_picker, null));
        builder.setPositiveButton(getString(android.R.string.ok), null);
        builder.setNegativeButton(getString(android.R.string.cancel), null);
        builder.setTitle(getTitle(calendar, yearMonthDay[0], yearMonthDay[1], yearMonthDay[2]));

        final AlertDialog alertDialog = builder.show();

        final DatePicker datePicker = (DatePicker) alertDialog.findViewById(R.id.date_picker);
        datePicker.init(yearMonthDay[0], yearMonthDay[1], yearMonthDay[2], new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                alertDialog.setTitle(getTitle(calendar, year, monthOfYear, dayOfMonth));
            }
        });

        // OK Button
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                alertDialog.dismiss();
            }
        });

        // Background duplication fix for api 21+
        if (null != alertDialog.getWindow()) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    public void showDatePickerDialog() {
        Date date = new Date();

        if (isEnterBirth) {
            String Birth = userBirth.getText().toString();
            String[] birth = Birth.split("/");


            date.setDate(Integer.parseInt(birth[0]));
            date.setMonth(Integer.parseInt(birth[1]) - 1);
            String year = "";

            for (int i = 0; i < birth[2].length() - 1; i++) {
                year = year + birth[2].charAt(i);
            }

            date.setYear(Integer.parseInt(year) - 1900);
        } else {
            date = DEFAULT_BIRTHDAY_DATE.getTime();
        }
        showDatePicker(date);
    }

    private DatePickerDialog.OnDateSetListener mDateListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    updateDate(year, month, day);
                }
            };

    public void updateDate(int year, int month, int day) {
        isEnterBirth = true;
        month = month + 1;
        userBirth.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(day).append("/")
                .append(month).append("/")
                .append(year).append(" "));
    }

    private String getTitle(Calendar mCalendar, int year, int month, int day) {

        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.DAY_OF_MONTH, day);
        return DateUtils.formatDateTime(getActivity(),
                mCalendar.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE
                        | DateUtils.FORMAT_SHOW_WEEKDAY
                        | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_ABBREV_MONTH
                        | DateUtils.FORMAT_ABBREV_WEEKDAY);

    }

    private int[] ymdTripleFor(Date date) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(date);
        return new int[]{cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)};
    }

    private void closeFragment() {
        AnswerAsyncTask answerAsyncTask = new AnswerAsyncTask(new AnswerAsyncTaskInerface() {
            @Override
            public void processFinish() {
                RatingAsyncTask ratingAsyncTask = new RatingAsyncTask(new RatingAsyncTaskInteface() {
                    @Override
                    public void processFinish() {
                        progressDialog.dismiss();
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragmentContainer, new AdtvQuestionFragment());
                        transaction.commit();
                    }
                });
                ratingAsyncTask.execute();
            }
        });
        answerAsyncTask.execute();

    }
}
