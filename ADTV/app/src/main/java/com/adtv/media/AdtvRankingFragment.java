package com.adtv.media;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.adtv.media.adapters.RatingAdapter;
import com.adtv.media.other.Globals;

import static com.adtv.media.AdtvQuestionFragment.KEY_GAME_STARTED;

/**
 * Created by Георгий on 11.03.2015.
 */
public class AdtvRankingFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ranking_adtv, container, false);
        Button mButtonBack = (Button) view.findViewById(R.id.fragment_ranking_button_back);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        ListView listView = (ListView) view.findViewById(R.id.fragment_ranking_list);
        if (Globals.rating == null || Globals.rating.getUser_place() == null) {
            Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.rating_not_found), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return view;
        } else {
            if (PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getBoolean(KEY_GAME_STARTED, false)){
                final RatingAdapter ratingAdapter = new RatingAdapter(Globals.rating, getActivity());
                listView.setAdapter(ratingAdapter);
            }
            return view;
        }
    }
}
