package com.adtv.media;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.adtv.media.adapters.PrizesAdapter;
import com.adtv.media.other.Globals;

/**
 * Created by Георгий on 11.03.2015.
 */
public class AdtvPrizesFragment extends Fragment {

    ListView lPrizes;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prizes_adtv, container, false);
        Button mButtonBack = (Button) view.findViewById(R.id.fragment_prizes_button_back);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        lPrizes = (ListView) view.findViewById(R.id.list_prizes);
        if (!Globals.compastionIsAnswerPresent()) {
            Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.prizes_not_found), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return view;
        } else {
            final PrizesAdapter answerAdapter = new PrizesAdapter(Globals.competitions, getActivity());
            lPrizes.setAdapter(answerAdapter);
            return view;
        }
    }
}
