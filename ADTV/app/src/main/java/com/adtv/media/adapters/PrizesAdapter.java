package com.adtv.media.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.adtv.media.R;
import com.adtv.media.dto.Competitions;
import com.adtv.media.dto.Prizes;
import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by kgb on 20.06.2015.
 */
public class PrizesAdapter extends BaseAdapter {

    Competitions competitions;
    LayoutInflater inflater;
    Context context;
    List<Prizes> prizesList = new ArrayList<>();

    public PrizesAdapter(Competitions competitions, Context context) {
        this.competitions = competitions;
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);
        this.prizesList = competitions.getPrizes();
    }

    @Override
    public int getCount() {
        return prizesList.size();
    }

    @Override
    public Object getItem(int position) {
        return prizesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.list_item_prize, parent, false);

        AQuery aq = new AQuery(context).recycle(convertView);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.prizes_image_src);
        String imgUri = URLConstants.BASE_URL.concat(prizesList.get(position).getImage());
        Log.i("IMAGE:", imgUri);
        //aq.id(imageView).image(imgUri, true, true);
        aq.id(imageView).image(imgUri, true, true, 0, 0, new BitmapAjaxCallback() {
            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                iv.setImageBitmap(Globals.getRoundedCornerImage(bm, 10, 0, context));
            }
        });

        TextView textView1 = (TextView) convertView.findViewById(R.id.prizes_text_src);
        textView1.setText(prizesList.get(position).getDescription());

        TextView textView2 = (TextView) convertView.findViewById(R.id.prizes_text_desc);
        textView2.setText(prizesList.get(position).getTitle());

        Button button = (Button) convertView.findViewById(R.id.prizes_num_prize);
        button.setText(Integer.toString(position + 1));
        return convertView;
    }

}
