package com.adtv.media.dto;

/**
 * Created by kgb on 17.06.2015.
 */
public class Address {
    private Integer id;
    private String street = "";
    private String building_name = "";
    private String building_number = "";

    public Address() {
    }

    public Address(Integer id, String street, String building_name, String building_number) {
        this.id = id;
        this.street = street;
        this.building_name = building_name;
        this.building_number = building_number;
    }

    @Override
    public String toString() {
        return "" + street + "" + building_name + "" + building_number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding_name() {
        return building_name;
    }

    public void setBuilding_name(String building_name) {
        this.building_name = building_name;
    }

    public String getBuilding_number() {
        return building_number;
    }

    public void setBuilding_number(String building_number) {
        this.building_number = building_number;
    }
}
