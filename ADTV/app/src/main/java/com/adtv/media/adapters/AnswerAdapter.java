package com.adtv.media.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.adtv.media.R;
import com.adtv.media.dto.AnswerForm;
import com.adtv.media.dto.Competitions;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by kgb on 20.06.2015.
 */
public class AnswerAdapter extends BaseAdapter {

    Competitions competitions;
    LayoutInflater inflater;
    Context context;
    List<AnswerForm> answersList = new ArrayList<>();

    public AnswerAdapter(Competitions competitions, Context context) {
        this.competitions = competitions;
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);
        answersList.add(new AnswerForm("A", competitions.getAnswers().getA()));
        answersList.add(new AnswerForm("B", competitions.getAnswers().getB()));
        answersList.add(new AnswerForm("C", competitions.getAnswers().getC()));
        answersList.add(new AnswerForm("D", competitions.getAnswers().getD()));
    }

    @Override
    public int getCount() {
        return answersList.size();
    }

    @Override
    public Object getItem(int position) {
        return answersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (competitions.isClosed()) {
            convertView = inflater.inflate(R.layout.list_answerd, parent, false);
        } else {
            convertView = inflater.inflate(R.layout.list_answer, parent, false);
        }
        TextView textView1 = (TextView) convertView.findViewById(R.id.answer_text);
        //textView1.setText(answersList.get(position).getId() +": "+answersList.get(position).getTitle());
        textView1.setText("" + answersList.get(position).getTitle());
        if (answersList.get(position).getId().toLowerCase().equals(competitions.getIs_answered().toLowerCase())) {
            textView1.setTextColor(context.getResources().getColor(R.color.green_dark));
            textView1.setTypeface(null, Typeface.BOLD);
            textView1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        }
        return convertView;
    }
}
