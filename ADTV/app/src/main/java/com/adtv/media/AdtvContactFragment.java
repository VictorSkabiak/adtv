package com.adtv.media;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Георгий on 06.03.2015.
 */
public class AdtvContactFragment extends BaseFragment {

    private EditText titleTextEdit;
    private EditText messageEditText;
    private Button mButtonSendMessage;

    private sendContact mSendContact;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contact_adtv, container, false);

        Button mButtonBack = (Button) v.findViewById(R.id.fragment_contact_button_back);
        mButtonBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        titleTextEdit = (EditText) v.findViewById(R.id.titleTextEdit);
        messageEditText = (EditText) v.findViewById(R.id.messageEditText);

        mButtonSendMessage = (Button) v.findViewById(R.id.fragment_contact_button_send_message);
        mButtonSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (titleTextEdit.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.required), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                if (messageEditText.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.required), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }
                mButtonSendMessage.setEnabled(false);
                showLoadingDialog(getString(R.string.sending));
                mSendContact = new sendContact();
                mSendContact.execute();
            }
        });
        return v;
    }

    public class sendContact extends AsyncTask<Void, Void, String> {
        private final String url = URLConstants.CONTACT_US;

        private boolean mIsCanceled;

        public void cancelRequest() {
            mIsCanceled = true;
        }

        sendContact() {
            mIsCanceled = false;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... voids) {
            String request = "";
            if (Globals.userRegistered != null) {
                JSONObject json = new JSONObject();
                try {
                    json.put("user_id", Globals.userRegistered.getId());
                    json.put("token", Globals.userRegistered.getToken());
                    json.put("title", titleTextEdit.getText().toString());
                    json.put("message", messageEditText.getText().toString());
                    Log.i("REQUEST:", json.toString());
                    request = UrlConnectionUtils.postData(url, json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return request;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (null == getActivity() || null == getActivity().getResources()) {
                return;
            }
            mButtonSendMessage.setEnabled(true);

            if (mIsCanceled) {
                return;
            }

            hideLoadingDialog();
            Toast toast = Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.message_send), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            getActivity().onBackPressed();
        }
    }

    @Override
    protected DialogInterface.OnDismissListener getDialogOnDismissListener() {
        return new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (null != mSendContact) {
                    mButtonSendMessage.setEnabled(true);
                    mSendContact.cancel(false);
                    mSendContact.cancelRequest();
                }
            }
        };
    }
}
