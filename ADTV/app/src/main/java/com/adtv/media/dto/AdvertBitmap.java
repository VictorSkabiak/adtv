package com.adtv.media.dto;

/**
 * Created by kgb on 23.06.2015.
 */
public class AdvertBitmap {
    private String id;
    private String name;
    private String image;
    private String url;
    private String timing;

    public AdvertBitmap() {

    }

    public AdvertBitmap(Integer id, String name, String image, String url, Integer timing) {
        this.id = String.valueOf(id);

        this.image = image;
        this.url = url;
        this.timing = String.valueOf(timing);
    }

    @Override
    public String toString() {
        return "AdvertBitmap{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", url='" + url + '\'' +
                ",timing='" + timing + '\'' +
                '}';
    }


    public Integer getTiming() {
        String id_1 = "";
        if (timing == "") {
            id_1 = "-1";
        } else {
            id_1 = timing;
        }
        return Integer.parseInt(id_1);
    }

    public void setTiming(Integer timing) {
        this.timing = String.valueOf(timing);
    }

    public Integer getId() {
        String id_1 = "";
        if (id == "") {
            id_1 = "-1";
        } else {
            id_1 = id;
        }
        return Integer.parseInt(id_1);
    }


    public void setId(Integer id) {
        this.id = String.valueOf(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

}
