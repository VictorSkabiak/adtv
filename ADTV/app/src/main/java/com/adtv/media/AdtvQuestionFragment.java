package com.adtv.media;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adtv.media.async.AnswerAsyncTask;
import com.adtv.media.async.AnswerAsyncTaskInerface;
import com.adtv.media.async.RatingAsyncTask;
import com.adtv.media.async.RatingAsyncTaskInteface;
import com.adtv.media.other.Globals;
import com.adtv.media.other.NetworkUtils;

/**
 * Created by Георгий on 06.03.2015.
 */
public class AdtvQuestionFragment extends BaseFragment {

    public static final String KEY_GAME_STARTED = "key_game_started";

    private static final long GLOBAL_RATING_UPDATE_TIME = 180000; // 3 minutes

    String tag = "AdtvQuestionFragment";
    ImageView imageView;
    AnswerAsyncTask answerAsyncTask;
    private TextView globalRatingTextView;
    View glView;
    private RatingAsyncTask ratingAsyncTask;
    private CountDownTimer mCountDownTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_question_adtv, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getActivity().finish();
                    System.exit(0);
                    return true;
                } else {
                    return false;
                }
            }
        });
        glView = view;

        imageView = (ImageView) view.findViewById(R.id.image_question);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAllTasks();
                showLoadingDialog(getString(R.string.loading));
                answerAsyncTask = new AnswerAsyncTask(new AnswerAsyncTaskInerface() {
                    @Override
                    public void processFinish() {
                        ratingAsyncTask = new RatingAsyncTask(new RatingAsyncTaskInteface() {
                            @Override
                            public void processFinish() {
                                hideLoadingDialog();
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                transaction.replace(R.id.fragmentContainer, new AdtvAnswerFragment(), "fragment_answer");
                                transaction.addToBackStack(null);
                                transaction.commit();
                            }
                        });
                        ratingAsyncTask.execute();
                    }
                });
                answerAsyncTask.execute();

            }
        });

        globalRatingTextView = (TextView) glView.findViewById(R.id.textOne);

        Button mContactButton = (Button) view.findViewById(R.id.fragment_question_button_contacts);
        mContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentContainer, new AdtvContactFragment(), "fragment_about");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        Button mInfoButton = (Button) view.findViewById(R.id.fragment_question_button_info);
        mInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentContainer, new AdtvInfoFragment(), "fragment_info");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        Button mSettingsButton = (Button) view.findViewById(R.id.fragment_question_button_settings);
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentContainer, new AdtvSettingsFragment(), "fragment_settings");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        Button mRankingButton = (Button) view.findViewById(R.id.fragment_question_button_ranking);
        mRankingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelAllTasks();
                showLoadingDialog(getString(R.string.loading));
                ratingAsyncTask = new RatingAsyncTask(new RatingAsyncTaskInteface() {
                    @Override
                    public void processFinish() {
                        hideLoadingDialog();
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragmentContainer, new AdtvRankingFragment(), "fragment_ranking");
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });
                ratingAsyncTask.execute();
            }
        });
        Button mPrizeButton = (Button) view.findViewById(R.id.fragment_question_button_prize);
        mPrizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentContainer, new AdtvPrizesFragment(), "fragment_prize");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        if (!new NetworkUtils(getActivity().getApplicationContext()).isConnectionAvailable()) {
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (null != mCountDownTimer) {
            mCountDownTimer.cancel();
        }

        mCountDownTimer = new CountDownTimer(GLOBAL_RATING_UPDATE_TIME, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Log.d("GlobalRatingUpdate", " mCountDownTimer = " + mCountDownTimer.toString());
                Log.d("GlobalRatingUpdate", " mCountDownTimer :: onFinish() -> updating");
                updateGlobalRating();
                updateQuestions();
                mCountDownTimer.start();
            }
        }.start();

        if (PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getBoolean(KEY_GAME_STARTED, false)) {
            updateGlobalRating();
        } else {
            globalRatingTextView.setVisibility(View.GONE);
        }

        updateQuestions();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (null != mCountDownTimer) {
            mCountDownTimer.cancel();
        }
    }


    @Override
    protected DialogInterface.OnDismissListener getDialogOnDismissListener() {
        return new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                cancelAllTasks();
            }
        };
    }

    private void updateGlobalRating() {
        new RatingAsyncTask(new RatingAsyncTaskInteface() {
            @Override
            public void processFinish() {
                if (null != Globals.rating && null != Globals.rating.getUser_place()
                        && !TextUtils.isEmpty(String.valueOf(Globals.rating.getUser_place()))
                        && !(String.valueOf(Globals.rating.getUser_place())).toLowerCase().equals("null")) {
                    Log.d("GlobalRatingUpdate", "Rating updated = " + Globals.rating.getUser_place());
                    globalRatingTextView.setVisibility(View.VISIBLE);
                    globalRatingTextView.setText(String.valueOf(Globals.rating.getUser_place()));
                } else {
                    globalRatingTextView.setVisibility(View.GONE);
                }
            }
        }).execute();
    }

    private void updateQuestions() {
        answerAsyncTask = new AnswerAsyncTask(new AnswerAsyncTaskInerface() {
            @Override
            public void processFinish() {
                if (!Globals.compastionIsAnswerPresent() || Globals.competitions.isClosed()) {
                    imageView.setImageResource(R.drawable.round_3);
                } else {
                    imageView.setImageResource(R.drawable.round_3_with_label);
                }
            }
        });
        answerAsyncTask.execute();
    }

    private void cancelAllTasks() {
        if (null != answerAsyncTask) {
            answerAsyncTask.cancelRequest();
            answerAsyncTask.cancel(false);
            answerAsyncTask = null;
        }

        if (null != ratingAsyncTask) {
            ratingAsyncTask.cancelRequest();
            ratingAsyncTask.cancel(false);
            ratingAsyncTask = null;
        }
    }
}
