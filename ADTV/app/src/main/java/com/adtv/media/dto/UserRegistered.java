package com.adtv.media.dto;

/**
 * Created by kgb on 19.06.2015.
 */
public class UserRegistered {
    private Integer id;
    private String token;

    public UserRegistered() {
    }

    public UserRegistered(Integer id, String token) {
        this.id = id;
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
