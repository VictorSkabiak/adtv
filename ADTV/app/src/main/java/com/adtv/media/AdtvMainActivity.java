package com.adtv.media;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.adtv.media.async.AnswerAsyncTask;
import com.adtv.media.async.AnswerAsyncTaskInerface;
import com.adtv.media.async.RatingAsyncTask;
import com.adtv.media.async.RatingAsyncTaskInteface;
import com.adtv.media.dto.UserRegistered;
import com.adtv.media.other.Globals;

import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by Георгий on 05.03.2015.
 */
public class AdtvMainActivity extends FragmentActivity {

    public static Locale myLocale;
    private PendingIntent pendingIntent;
    private ScheduledExecutorService scheduleTaskExecutor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userIsRegistered();
        scheduleTaskExecutor = Executors.newSingleThreadScheduledExecutor();
        /*scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                                                     @Override
                                                     public void run() {
                                                        Log.i("SCHEDULE START LOAD","<-----------");
                                                         loadAllDataSchedule();
                                                     }
                                                 },1,5, TimeUnit.MINUTES);*/

        setContentView(R.layout.activity_main_adtv);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {

            /*if(Globals.advertBitmap.getId()!=null && Globals.advertBitmap.getImage().toString()!=null &&Globals.advertBitmap.getName().toString()!=null
                    && Globals.advertBitmap.getUrl().toString() != null && Globals.advertBitmap.getTiming()!=null && Globals.advertBitmap.getTiming()!=0) {
               */
            if (Globals.advertBitmap != null) {
                if (Globals.advertBitmap.getId() != -1 && Globals.advertBitmap.getImage().toString() != "" && Globals.advertBitmap.getName().toString() != ""
                        && Globals.advertBitmap.getUrl().toString() != "" && Globals.advertBitmap.getTiming() != -1 && Globals.advertBitmap.getTiming() != 0) {

                    fragment = new AdtvAdverFragment();
                    fm.beginTransaction()
                            .add(R.id.fragmentContainer, fragment, "fragment_advert")
                            .commit();
                } else {
                    initFragment();
                }

            } else {
                initFragment();
            }
        }
    }


    private void initFragment() {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        if (((AdtvMainActivity) this).userIsRegistered()) {
            transaction.replace(R.id.fragmentContainer, new AdtvQuestionFragment());
        } else {
            transaction.replace(R.id.fragmentContainer, new AdtvLoginFragment());
        }
        transaction.commit();
    }

    public void restartApp() {

        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void logoutUser() {
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.remove("user_id");
        ed.remove("user_token");
        ed.commit();
    }

    public void registerUser(Integer id, String token) {
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putInt("user_id", id);
        ed.putString("user_token", token);
        ed.commit();
        UserRegistered userRegistered = new UserRegistered(id, token);
        Globals.userRegistered = userRegistered;
    }

    public boolean userIsRegistered() {
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        Integer id = prefs.getInt("user_id", 0);
        Log.i("REGISTER:------------->", "user_id: " + id);
        if (id == 0) {
            return false;
        }
        UserRegistered userRegistered = new UserRegistered();
        userRegistered.setId(id);
        Log.i("REGISTER------------->:", "user_token: " + prefs.getString("user_token", ""));
        userRegistered.setToken(prefs.getString("user_token", ""));
        Globals.userRegistered = userRegistered;
        return true;
    }

    protected void restart() {
        pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(this.getIntent()), 0);
        AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, pendingIntent);
        System.exit(1);
    }

    private void loadAllDataSchedule() {
        if (userIsRegistered()) {
            AnswerAsyncTask answerAsyncTask = new AnswerAsyncTask(new AnswerAsyncTaskInerface() {
                @Override
                public void processFinish() {
                    RatingAsyncTask ratingAsyncTask = new RatingAsyncTask(new RatingAsyncTaskInteface() {
                        @Override
                        public void processFinish() {
                            Log.i("SCHEDULE END LOAD", "<------------");
                        }
                    });
                    ratingAsyncTask.execute();
                }
            });
            answerAsyncTask.execute();
        } else {
            Log.i("SCHEDULE NO USER FOUND", "<------------");
        }
    }

}
