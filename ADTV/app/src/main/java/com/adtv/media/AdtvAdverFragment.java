package com.adtv.media;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;

/**
 * Created by Георгий on 10.03.2015.
 */
public class AdtvAdverFragment extends Fragment {

    int width = 800;
    int height = 1080;
    String urlBanner;
    CountDownTimer timer;
    int i;

    private void getSizeScreen() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.width = size.x;
        this.height = size.y;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getSizeScreen();

        i = 0;
        View v = inflater.inflate(R.layout.fragment_advertising_adtv, container, false);
        ImageView mImageView = (ImageView) v.findViewById(R.id.advert_image);

        AQuery aq = new AQuery(getActivity());
        String paramImage = "imagefly/w" + width + "/";
        String imgUri = URLConstants.BASE_URL.concat(paramImage).concat(Globals.advertBitmap.getImage());
        aq.id(mImageView).image(imgUri, true, true, 0, 0);
        urlBanner = Globals.advertBitmap.getUrl();

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(urlBanner)) {
                    String url = (!urlBanner.startsWith("http://") && !urlBanner.startsWith("https://")) ? "http://" + urlBanner : urlBanner;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    if (URLUtil.isValidUrl(url)) {
                        i.setData(Uri.parse(url));
                        hideFragment();
                        startActivity(i);
                    }
                }
            }
        });
        Button mCloseButton = (Button) v.findViewById(R.id.continue_button);
        //long time = Globals.advertBitmap.getTiming().longValue();

        timer = new CountDownTimer((Globals.advertBitmap.getTiming().longValue()) * 1000 + 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                // text.setText("РћСЃС‚Р°Р»РѕСЃСЊ: " + (millisUntilFinished / 1000 -1));
                if ((millisUntilFinished / 1000 - 1) <= 0 && i == 0) {

                    if (getActivity() != null) {
                        hideFragment();
                    }
                }
            }

            public void onFinish() {


            }
        };
        timer.start();


        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //bitmap.recycle();

                i = 1;
                hideFragment();
            }
        });
        return v;
    }

    private void hideFragment() {
        timer.onFinish();
        timer.cancel();
        if (!isResumed()) {
            return;
        }
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (transaction != null)
            if (((AdtvMainActivity) getActivity()).userIsRegistered()) {
                transaction.replace(R.id.fragmentContainer, new AdtvQuestionFragment());
            } else {
                transaction.replace(R.id.fragmentContainer, new AdtvLoginFragment());
            }
        transaction.commit();
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
