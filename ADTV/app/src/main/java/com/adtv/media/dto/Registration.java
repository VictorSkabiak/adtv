package com.adtv.media.dto;

/**
 * Created by kgb on 17.06.2015.
 */
public class Registration {
    private String success;
    private Integer user_id;
    private String token;
    private Error errors;

    public Registration() {
    }

    public Registration(String success, Integer user_id, String token, Error errors) {
        this.success = success;
        this.user_id = user_id;
        this.token = token;
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "success='" + success + '\'' +
                ", user_id=" + user_id +
                ", token='" + token + '\'' +
                '}';
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Error getErrors() {
        return errors;
    }

    public void setErrors(Error errors) {
        this.errors = errors;
    }
}
