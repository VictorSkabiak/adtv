package com.adtv.media.async;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.adtv.media.dto.GlobalRating;
import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by WorkPC on 11.11.2016.
 */

public class GlobalRatingAsyncTask extends AsyncTask<Void, Void, String> {

    private GlobalRatingAsyncTaskInterface mGlobalRatingAsyncTaskInterface;

    public GlobalRatingAsyncTask(GlobalRatingAsyncTaskInterface globalRatingAsyncTaskInterface) {
        mGlobalRatingAsyncTaskInterface = globalRatingAsyncTaskInterface;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String request = "";
        if (Globals.userRegistered != null) {
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", Globals.userRegistered.getId());
                json.put("token", Globals.userRegistered.getToken());
                request = UrlConnectionUtils.postData(URLConstants.GET_GLOBAL_RATING, json);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return request;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            JsonParser parser = new JsonParser();
            if (TextUtils.isEmpty(s)) {
                Globals.globalRating = new GlobalRating();
                mGlobalRatingAsyncTaskInterface.processFinish();
                return;
            }
            JsonObject rootObject = parser.parse(s).getAsJsonObject();
            JsonElement elementGlobalRating = rootObject.getAsJsonObject("response").getAsJsonObject("rating");

            Gson gson = new GsonBuilder().create();
            if (elementGlobalRating.isJsonObject()) {
                GlobalRating globalRating = gson.fromJson(elementGlobalRating, GlobalRating.class);
                if (globalRating != null) {
                    Globals.globalRating = globalRating;
                }
            }
            mGlobalRatingAsyncTaskInterface.processFinish();

        } catch (Exception e) {
            Globals.globalRating = new GlobalRating();
            mGlobalRatingAsyncTaskInterface.processFinish();
            return;
        }
    }
}
