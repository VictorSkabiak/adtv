package com.adtv.media.async;

import android.os.AsyncTask;
import android.util.Log;

import com.adtv.media.dto.Registration;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

/**
 * Created by kgb on 27.06.2015.
 */
public class RegisterAsyncTask extends AsyncTask<Void, Void, String> {

    private RegisterAsyncTaskInterface registerAsyncTaskInterface = null;
    private String userName;
    private String userSurname;
    private Integer userAddress;
    private Integer userGender;
    private String userBirth;
    private String userEmail;
    private String userPhone;

    public RegisterAsyncTask() {
    }

    public RegisterAsyncTask(RegisterAsyncTaskInterface registerAsyncTaskInterface,
                             String userName,
                             String userSurname,
                             Integer userAddress,
                             Integer userGender,
                             String userBirth,
                             String userEmail,
                             String userPhone) {
        this.registerAsyncTaskInterface = registerAsyncTaskInterface;
        this.userAddress = userAddress;
        this.userBirth = userBirth;
        this.userEmail = userEmail;
        this.userGender = userGender;
        this.userName = userName;
        this.userSurname = userSurname;
        this.userPhone = userPhone;
    }

    @Override
    protected String doInBackground(Void... voids) {
        JSONObject json = new JSONObject();
        String request = "";
        try {
            json.put("name", userName);
            json.put("surname", userSurname);
            json.put("address_id", userAddress);
            json.put("gender", userGender);
            json.put("date_of_birth", userBirth);
            json.put("email", userEmail);
            json.put("phone", userPhone);
            request = UrlConnectionUtils.postData(URLConstants.USER_REGISTRATION, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Registration registration = null;
        try {
            String json;
            JSONObject jsonObject = new JSONObject(s);
            json = jsonObject.getString("response");

            Type type = new TypeToken<Registration>() {
            }.getType();
            registration = new Gson().fromJson(json, type);
        } catch (JSONException e) {
            Log.d("myLog", "GetCitiesAsyncTask error is: " + e.getMessage());
        } finally {
            registerAsyncTaskInterface.processFinish(registration);
        }
    }
}
