package com.adtv.media.async;

import com.adtv.media.dto.Registration;

/**
 * Created by kgb on 27.06.2015.
 */
public interface RegisterAsyncTaskInterface {
    void processFinish(Registration registration);
}
