package com.adtv.media.dto;

/**
 * Created by kgb on 20.06.2015.
 */
public class Answers {
    private String A;
    private String B;
    private String C;
    private String D;

    public Answers() {
    }

    public Answers(String a, String b, String c, String d) {
        A = a;
        B = b;
        C = c;
        D = d;
    }

    @Override
    public String toString() {
        return "Answers{" +
                "A='" + A + '\'' +
                ", B='" + B + '\'' +
                ", C='" + C + '\'' +
                ", D='" + D + '\'' +
                '}';
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public String getC() {
        return C;
    }

    public void setC(String c) {
        C = c;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }
}
