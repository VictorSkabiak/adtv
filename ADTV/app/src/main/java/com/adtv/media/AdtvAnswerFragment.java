package com.adtv.media;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adtv.media.adapters.AnswerAdapter;
import com.adtv.media.dto.AnswerForm;
import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by kgb on 20.06.2015.
 */
public class AdtvAnswerFragment extends Fragment {

    ListView lAnswer;
    FrameLayout mButtonBack;
    Button mButtonBackB;
    AnswerForm selectedAnswer;
    TextView alreadyAnswer;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_answer_adtv, container, false);
        mButtonBack = (FrameLayout) view.findViewById(R.id.fragment_answer_button_back_frame);
        lAnswer = (ListView) view.findViewById(R.id.list_answer);
        alreadyAnswer = (TextView) view.findViewById(R.id.text_is_answered);
        mButtonBackB = (Button) view.findViewById(R.id.fragment_answer_button_back);

        mButtonBackB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        if (!Globals.compastionIsAnswerPresent()) {
            Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.answer_not_found), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return view;
        } else {
            if ((Globals.competitions.isClosed())) {
                alreadyAnswer.setText(getResources().getString(R.string.already_answer));
                setGameStarted(true);
            }
            final AnswerAdapter answerAdapter = new AnswerAdapter(Globals.competitions, getActivity());
            lAnswer.setAdapter(answerAdapter);
            lAnswer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    int x = 0;
                    x = x + 1;
                    /*if (Integer.parseInt(Globals.competitions.getIs_answered()) == position) {
                        Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.yes_answer), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_answer), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }*/
                    selectedAnswer = (AnswerForm) answerAdapter.getItem(position);
                    if (!Globals.competitions.isClosed()) {
                        new sendAnswer().execute();
                    }
                }
            });
            return view;
        }
    }

    public class sendAnswer extends AsyncTask<Void, Void, String> {
        private final String url = URLConstants.SUBMIT_ANSWER_VARIANT;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), getResources().getString(R.string.loading),
                    getResources().getString(R.string.pleas_wait), false, false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            String request = "";
            if (Globals.userRegistered != null) {
                JSONObject json = new JSONObject();
                try {
                    json.put("user_id", Globals.userRegistered.getId());
                    json.put("token", Globals.userRegistered.getToken());
                    json.put("game_id", Globals.competitions.getGame_id());
                    json.put("variant", selectedAnswer.getId());
                    Log.i("REQUEST:", json.toString());
                    request = UrlConnectionUtils.postData(url, json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return request;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.indexOf("false") >= 0) {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.yes_answer), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.no_answer), Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
            Globals.competitions.setIs_answered(selectedAnswer.getId());
            setGameStarted(true);
            getActivity().onBackPressed();
        }
    }

    private void setGameStarted(boolean isStarted) {
        PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).edit().putBoolean(AdtvQuestionFragment.KEY_GAME_STARTED, isStarted).commit();
    }
}
