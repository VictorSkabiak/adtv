package com.adtv.media.dto;

/**
 * Created by kgb on 17.06.2015.
 */
public class Error {
    String email;

    public Error() {
    }

    public Error(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
