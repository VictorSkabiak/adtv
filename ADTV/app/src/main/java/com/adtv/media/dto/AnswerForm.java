package com.adtv.media.dto;

/**
 * Created by kgb on 20.06.2015.
 */
public class AnswerForm {
    private String id;
    private String title;

    public AnswerForm() {
    }

    public AnswerForm(String id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public String toString() {
        return "AnswerForm{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
