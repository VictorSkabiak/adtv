package com.adtv.media.other;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.TypedValue;

import com.adtv.media.dto.AdvertBitmap;
import com.adtv.media.dto.City;
import com.adtv.media.dto.Competitions;
import com.adtv.media.dto.GlobalRating;
import com.adtv.media.dto.Rating;
import com.adtv.media.dto.UserRegistered;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kgb on 17.06.2015.
 */
public class Globals {
    public static List<City> cityList = new ArrayList<>();
    public static UserRegistered userRegistered;
    public static Competitions competitions = new Competitions();
    public static Rating rating = new Rating();
    public static GlobalRating globalRating = new GlobalRating();
    public static AdvertBitmap advertBitmap;
    public static Bitmap advertImage;


    public static boolean compastionIsAnswerPresent() {
        return Globals.competitions != null && Globals.competitions.getAnswers() != null;
    }


    public static Bitmap getRoundedCornerImage(Bitmap bitmap, int cornerDips, int borderDips, Context context) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int borderSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) borderDips,
                context.getResources().getDisplayMetrics());
        final int cornerSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) cornerDips,
                context.getResources().getDisplayMetrics());
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);


        paint.setAntiAlias(true);
        //paint.setColor(0xFFFFFFFF);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);


        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);


        paint.setColor((Color.WHITE)); // you can change color of your border here, to other color
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((float) borderSizePx);
        canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

        return output;
    }
}
