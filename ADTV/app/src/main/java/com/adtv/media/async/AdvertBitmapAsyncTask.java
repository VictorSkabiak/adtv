package com.adtv.media.async;

import android.os.AsyncTask;
import android.util.Log;

import com.adtv.media.dto.AdvertBitmap;
import com.adtv.media.other.Globals;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.adtv.media.other.URLConstants.BANNER_UL;

/**
 * Created by kgb on 27.06.2015.
 */
public class AdvertBitmapAsyncTask extends AsyncTask<Void, Void, String> {

    private AdvertBitmapAsyncTaskInterface advertBitmapAsyncTaskInterface;

    public AdvertBitmapAsyncTask() {
    }

    public AdvertBitmapAsyncTask(AdvertBitmapAsyncTaskInterface advertBitmapAsyncTaskInterface) {
        this.advertBitmapAsyncTaskInterface = advertBitmapAsyncTaskInterface;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return UrlConnectionUtils.getGet(BANNER_UL);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            String json;


            JSONObject jsonObject = new JSONObject(s);
            if (JSONArray.class == jsonObject.get("response").getClass()) {

                JSONArray jsonArray = (JSONArray) jsonObject.get("response");

                if (jsonArray.length() > 0) {
                    json = ((JSONObject) jsonArray.get(0)).toString();
                    initJsonElement(json);
                } else {
                    initNullJsonElement();
                }
            } else {
                if (JSONObject.class == jsonObject.get("response").getClass()) {
                    json = jsonObject.getString("response");

                    initJsonElement(json);

                }
            }


        } catch (JSONException e) {
            Log.d("myLog", "GetCitiesAsyncTask error is: " + e.getMessage());
        } finally {
            advertBitmapAsyncTaskInterface.processFinish();
        }
    }

    private void initJsonElement(String json) {
        AdvertBitmap advertBitmap = new Gson().fromJson(json, AdvertBitmap.class);
        Globals.advertBitmap = advertBitmap;
    }


    private void initNullJsonElement() {
        AdvertBitmap advertBitmap = null;

        Globals.advertBitmap = advertBitmap;
    }

}
