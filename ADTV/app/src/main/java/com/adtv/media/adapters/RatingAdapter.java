package com.adtv.media.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.adtv.media.R;
import com.adtv.media.dto.Rating;
import com.adtv.media.dto.RatingUser;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by kgb on 20.06.2015.
 */
public class RatingAdapter extends BaseAdapter {

    Rating rating;
    LayoutInflater inflater;
    Context context;
    List<RatingUser> ratingList = new ArrayList<>();

    public RatingAdapter(Rating rating, Context context) {
        this.rating = rating;
        this.ratingList = rating.getList();
        this.context = context;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return ratingList.size();
    }

    @Override
    public Object getItem(int position) {
        return ratingList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.list_rating, parent, false);
        TextView textName = (TextView) convertView.findViewById(R.id.user_text);
        textName.setText(ratingList.get(position).getName());

        TextView textPosition = (TextView) convertView.findViewById(R.id.user_position);
        System.out.println(textPosition);
        System.out.println(ratingList.get(position).getPlace());
        textPosition.setText(Integer.toString(ratingList.get(position).getPlace()));

        return convertView;
    }
}
