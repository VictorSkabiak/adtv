package com.adtv.media.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kgb on 22.06.2015.
 */
public class Rating {
    Integer user_place;
    Integer total_players;
    List<RatingUser> list = new ArrayList<>();

    public Rating() {
    }

    public Rating(Integer user_place, Integer total_players, List<RatingUser> list) {
        this.user_place = user_place;
        this.total_players = total_players;
        this.list = list;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "user_place=" + user_place +
                ", total_players=" + total_players +
                ", list=" + list +
                '}';
    }

    public Integer getUser_place() {
        return user_place;
    }

    public void setUser_place(Integer user_place) {
        this.user_place = user_place;
    }

    public Integer getTotal_players() {
        return total_players;
    }

    public void setTotal_players(Integer total_players) {
        this.total_players = total_players;
    }

    public List<RatingUser> getList() {
        return list;
    }

    public void setList(List<RatingUser> list) {
        this.list = list;
    }
}
