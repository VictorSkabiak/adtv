package com.adtv.media;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.adtv.media.async.AdvertBitmapAsyncTask;
import com.adtv.media.async.AdvertBitmapAsyncTaskInterface;
import com.adtv.media.async.AnswerAsyncTask;
import com.adtv.media.async.AnswerAsyncTaskInerface;
import com.adtv.media.async.CitiesAsyncTask;
import com.adtv.media.async.CitiesAsyncTaskInterface;
import com.adtv.media.async.RatingAsyncTask;
import com.adtv.media.async.RatingAsyncTaskInteface;
import com.adtv.media.dto.UserRegistered;
import com.adtv.media.other.Globals;


public class AdtvSplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_adtv);
        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.linear_interpolator);
        Animation rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_progress);
        splash.startAnimation(rotate);

        getAdvert();
    }

    public boolean userIsRegistered() {
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        Integer id = prefs.getInt("user_id", 0);
        if (id == 0) {
            return false;
        }
        UserRegistered userRegistered = new UserRegistered(id, prefs.getString("user_token", ""));
        Globals.userRegistered = userRegistered;
        return true;
    }

    private void getAdvert() {
        AdvertBitmapAsyncTask advertBitmapAsyncTask = new AdvertBitmapAsyncTask(new AdvertBitmapAsyncTaskInterface() {
            @Override
            public void processFinish() {
                if (userIsRegistered()) {
                    getAnswer();
                } else {
                    getCity();
                }
            }
        });
        advertBitmapAsyncTask.execute();
    }

    private void getCity() {
        CitiesAsyncTask citiesAsyncTask = new CitiesAsyncTask(new CitiesAsyncTaskInterface() {
            @Override
            public void processFinish() {
                closeActivity();
            }
        });
        citiesAsyncTask.execute();
    }

    private void getAnswer() {
        AnswerAsyncTask answerAsyncTask = new AnswerAsyncTask(new AnswerAsyncTaskInerface() {
            @Override
            public void processFinish() {
                getRating();
            }
        });
        answerAsyncTask.execute();
    }

    private void getRating() {
        RatingAsyncTask ratingAsyncTask = new RatingAsyncTask(new RatingAsyncTaskInteface() {
            @Override
            public void processFinish() {
                closeActivity();
            }
        });
        ratingAsyncTask.execute();
    }

    private void closeActivity() {
        Intent i = new Intent(AdtvSplashActivity.this, AdtvMainActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        finish();
    }
}
