package com.adtv.media.dto;

/**
 * Created by WorkPC on 11.11.2016.
 */

public class GlobalRating {

    private int user_place;
    private String points;

    public GlobalRating() {
    }

    public GlobalRating(int user_place, String points) {

        this.user_place = user_place;
        this.points = points;
    }

    public int getUser_place() {
        return user_place;
    }

    public String getPoints() {
        return points;
    }
}
