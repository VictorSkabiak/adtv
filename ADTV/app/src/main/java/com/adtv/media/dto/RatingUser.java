package com.adtv.media.dto;

/**
 * Created by kgb on 22.06.2015.
 */
public class RatingUser {
    String name;
    Float points;
    Integer place;

    public RatingUser() {
    }

    public RatingUser(String name, Float points, Integer place) {
        this.name = name;
        this.points = points;
        this.place = place;
    }

    @Override
    public String toString() {
        return "RatingUser{" +
                "name='" + name + '\'' +
                ", points=" + points +
                ", place=" + place +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPoints() {
        return points;
    }

    public void setPoints(Float points) {
        this.points = points;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }
}
