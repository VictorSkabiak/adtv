package com.adtv.media.other;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;


/**
 * Created by Георгий on 13.03.2015.
 */

public class WhtsNotificationListener extends AccessibilityService {
    private static final String TAG = "Notifier";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "start");
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d(TAG, "onAccessibilityEvent");
        if (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
//            Notification notification = (Notification) event.getParcelableData();
//            Log.d(TAG, "notification: " + event.getText());
//            Log.d(TAG, "App: " + event.getPackageName());
//            Log.d(TAG, "Ticker: " + notification.tickerText);
//            int indexDelimiter = notification.ticker.indexOf(':');
//            String sender = ticker.substring(0, indexDelimiter);
//            String message = ticker.substring(indexDelimiter + 2);
        }
    }

    @Override
    public void onInterrupt() {
        Log.d(TAG, "onInterrupt");
    }

    @Override
    protected void onServiceConnected() {
        Log.d(TAG, "onServiceConnected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.notificationTimeout = 100;
        setServiceInfo(info);
    }
}
