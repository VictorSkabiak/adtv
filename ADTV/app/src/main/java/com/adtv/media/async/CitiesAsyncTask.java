package com.adtv.media.async;

import android.os.AsyncTask;
import android.util.Log;

import com.adtv.media.dto.City;
import com.adtv.media.other.Globals;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import static com.adtv.media.other.URLConstants.GET_CITIES;

/**
 * Created by kgb on 27.06.2015.
 */
public class CitiesAsyncTask extends AsyncTask<Void,Void,String> {

    private CitiesAsyncTaskInterface citiesAsyncTaskInterface = null;


    public CitiesAsyncTask() {
    }

    public CitiesAsyncTask(CitiesAsyncTaskInterface citiesAsyncTaskInterface) {
        this.citiesAsyncTaskInterface = citiesAsyncTaskInterface;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return UrlConnectionUtils.getGet(GET_CITIES);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {String json;
            JSONObject jsonObject=new JSONObject(s);
            json=jsonObject.getString("response");
            Type listCity = new TypeToken<List<City>>() {}.getType();
            Globals.cityList = new Gson().fromJson(json, listCity);
        } catch (JSONException e) {
            Log.d("myLog", "GetCitiesAsyncTask error is: " + e.getMessage());
        } finally {
            citiesAsyncTaskInterface.processFinish();
        }
    }
}
