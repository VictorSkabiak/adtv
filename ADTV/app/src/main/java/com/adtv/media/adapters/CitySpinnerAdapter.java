package com.adtv.media.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.adtv.media.R;
import com.adtv.media.dto.City;

import java.util.List;

/**
 * Created by Grant on 6/15/2015.
 */
public class CitySpinnerAdapter extends ArrayAdapter<City> {
    private Context context;
    private List<City> cities;
    private LayoutInflater inflater;

    public CitySpinnerAdapter(Context context, List<City> cities) {
        super(context, android.R.layout.simple_spinner_item, cities);
        this.context = context;
        cities.add(0, new City());
        this.cities = cities;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            //inflate your customlayout for the textview
            row = inflater.inflate(R.layout.spinner_item, parent, false);
        }
        //put the data in it
        City city = cities.get(position);
        if (city != null) {
            TextView text1 = (TextView) row.findViewById(android.R.id.text1);
            text1.setTextColor(Color.BLACK);
            text1.setText(city.getId() != null ? city.getName() : context.getString(R.string.selectCity));
        }

        return row;
//        City city=cities.get(position);
//        convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
//        TextView textView=(TextView) convertView.findViewById(android.R.id.text1);
//        String text=city.getId()!=null ? city.getTitle():context.getString(R.string.selectCity);
//        textView.setTextColor(Color.parseColor("#000000"));
//        textView.setText(text);
//        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        City city = cities.get(position);
        convertView = inflater.inflate(android.R.layout.select_dialog_singlechoice, parent, false);
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(city.getName());
        textView.setTextColor(Color.parseColor("#000000"));
        if (city.getId() == null) {
            return new View(context);
        }
        return convertView;
    }
}
