package com.adtv.media.other;

/**
 * Created by WorkPC on 08.11.2016.
 */

public class URLConstants {

    public static final String BASE_URL = "http://adtv-game.com/";
    public static final String BANNER_UL = BASE_URL + "api/banner/index";
    public static final String GET_CITIES = BASE_URL + "api/user/cities";
    public static final String GET_ADDRESSES = BASE_URL + "api/user/addresses";
    public static final String USER_REGISTRATION = BASE_URL + "api/user/register";
    public static final String CONTACT_US = BASE_URL + "api/user/contact";
    public static final String GET_ANSWER_VARIANTS_FOR_CURRENT_QUESTION = BASE_URL + "api/game/get";
    public static final String SUBMIT_ANSWER_VARIANT = BASE_URL + "api/game/submit";
    public static final String GET_RATING = BASE_URL + "api/game/getRating";
    public static final String GET_GLOBAL_RATING = BASE_URL + "api/game/globalRating";

}
