package com.adtv.media;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by WorkPC on 11.11.2016.
 */

public abstract class BaseFragment extends Fragment implements android.os.Handler.Callback {


    private static final int MSG_SHOW_LOADING_DIALOG = 0x1000;
    private static final int MSG_UPDATE_LOADING_MESSAGE = 0x1001;
    private static final int MSG_HIDE_LOADING_DIALOG = 0x1002;

    protected final android.os.Handler mUIHandler = new android.os.Handler(BaseFragment.this);
    private ProgressDialog mProgressDialog;

    public BaseFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    /* *********************************************************** */
    /* ********************* Handler.Callback ******************** */
    /* *********************************************************** */

    @Override
    public boolean handleMessage(Message message) {
        boolean result = false;
        switch (message.what) {
            case MSG_SHOW_LOADING_DIALOG:
                if (null == mProgressDialog) {
                    mProgressDialog = new ProgressDialog(getActivity());
                    mProgressDialog.setCanceledOnTouchOutside(false);
                    mProgressDialog.setOnDismissListener(getDialogOnDismissListener());
                }
                mProgressDialog.setMessage((String) message.obj);
                if (!mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }

                result = true;
                break;
            case MSG_UPDATE_LOADING_MESSAGE:
                if (null != mProgressDialog) {
                    mProgressDialog.setMessage((String) message.obj);
                }
                result = true;
                break;
            case MSG_HIDE_LOADING_DIALOG:
                if ((null != mProgressDialog) && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                result = true;
                break;
        }
        return result;
    }

    protected DialogInterface.OnDismissListener getDialogOnDismissListener() {
        return new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Do nothing
            }
        };
    }

    /* **************************************************************** */
    /* ************************** Utility API ************************* */
    /* **************************************************************** */

    protected void removeFragment(String tag) {
        FragmentManager manager = getFragmentManager();
        if (manager == null) {
            return;
        }
        Fragment hideFragment = manager.findFragmentByTag(tag);
        if (hideFragment != null) {
            FragmentTransaction tr = manager.beginTransaction();
            tr.remove(hideFragment);
            tr.commit();
        }
        manager.popBackStack();
    }

    public void replaceFragment(int containerViewId, Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(containerViewId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }

        transaction.commit();
    }

    public void addFragment(int containerViewId, Fragment fragment, boolean addToBackStack) {
        String tag = fragment.getClass().getName();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(containerViewId, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    public void addFragment(int containerViewId, Fragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(containerViewId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    /**
     * Shows loading dialog.
     *
     * @param messageValue message value
     */
    public void showLoadingDialog(String messageValue) {
        Message message = new Message();
        message.what = MSG_SHOW_LOADING_DIALOG;
        message.obj = messageValue;
        mUIHandler.sendMessage(message);
    }

    /**
     * Updates loading dialog message.
     *
     * @param messageValue message value
     */
    public void updateLoadingDialogMessage(String messageValue) {
        Message message = new Message();
        message.what = MSG_UPDATE_LOADING_MESSAGE;
        message.obj = messageValue;
        mUIHandler.sendMessage(message);
    }

    /**
     * Hides loading dialog.
     */
    public void hideLoadingDialog() {
        Message message = new Message();
        message.what = MSG_HIDE_LOADING_DIALOG;
        mUIHandler.sendMessage(message);
    }

    /**
     * Show an error message
     *
     * @param title   A string representing an error title.
     * @param message A string representing an error.
     */
    public void showError(String title, String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public void showError(String title, String message, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", listener).create().show();
    }
}
