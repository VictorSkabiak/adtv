package com.adtv.media.dto;

/**
 * Created by kgb on 20.06.2015.
 */
public class Prizes {
    private String title;
    private String image;
    private String description;

    public Prizes() {
    }

    public Prizes(String title, String image, String description) {
        this.title = title;
        this.image = image;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
