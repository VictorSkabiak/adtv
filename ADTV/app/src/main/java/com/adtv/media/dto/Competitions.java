package com.adtv.media.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kgb on 20.06.2015.
 */
public class Competitions {
    private String current_time;
    private Date start_date;
    private String start_time;
    private Date end_date;
    private String end_time;
    private Integer game_id;
    private Answers answers;
    private List<Prizes> prizes = new ArrayList<>();
    private String is_answered;


    public Competitions() {
    }

    public Competitions(String current_time, Date start_date, String start_time, Date end_date, String end_time, Integer game_id, Answers answers, List<Prizes> prizes, String is_answered) {
        this.current_time = current_time;
        this.start_date = start_date;
        this.start_time = start_time;
        this.end_date = end_date;
        this.end_time = end_time;
        this.game_id = game_id;
        this.answers = answers;
        this.prizes = prizes;
        this.is_answered = is_answered;
    }

    public boolean isClosed() {
        return (getIs_answered() != null && !getIs_answered().equals("") && !getIs_answered().equals("false"));
    }

    @Override
    public String toString() {
        return "Competitions{" +
                "current_time=" + current_time +
                ", start_date=" + start_date +
                ", start_time=" + start_time +
                ", end_date=" + end_date +
                ", end_time=" + end_time +
                ", game_id=" + game_id +
                ", answers=" + answers +
                ", prizes=" + prizes +
                ", is_answered=" + is_answered +
                '}';
    }


    public String getIs_answered() {
        return is_answered;
    }

    public void setIs_answered(String is_answered) {
        this.is_answered = is_answered;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Integer getGame_id() {
        return game_id;
    }

    public void setGame_id(Integer game_id) {
        this.game_id = game_id;
    }

    public Answers getAnswers() {
        return answers;
    }

    public void setAnswers(Answers answers) {
        this.answers = answers;
    }

    public List<Prizes> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<Prizes> prizes) {
        this.prizes = prizes;
    }
}
