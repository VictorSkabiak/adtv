package com.adtv.media.other;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by vskabiak on 24.02.2016.
 */
public class NetworkUtils {

    private Context mContext;

    public NetworkUtils(Context context) {
        mContext = context;
    }

    public boolean isConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showConnectionErrorMassageInToast(int massageStringRes) {
        Toast.makeText(mContext, massageStringRes, Toast.LENGTH_SHORT).show();
    }
}
