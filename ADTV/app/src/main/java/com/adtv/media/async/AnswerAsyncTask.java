package com.adtv.media.async;

import android.os.AsyncTask;

import com.adtv.media.dto.Competitions;
import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by kgb on 23.06.2015.
 */
public class AnswerAsyncTask extends AsyncTask<Void, Void, String> {

    private AnswerAsyncTaskInerface inerface = null;
    private boolean mIsCanceled;

    public AnswerAsyncTask() {

    }

    public AnswerAsyncTask(AnswerAsyncTaskInerface inerface) {
        this.inerface = inerface;
        mIsCanceled = false;
    }

    public void cancelRequest() {
        mIsCanceled = true;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(Void... voids) {
        String request = "";
        if (Globals.userRegistered != null) {
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", Globals.userRegistered.getId());
                json.put("token", Globals.userRegistered.getToken());
                request = UrlConnectionUtils.postData(URLConstants.GET_ANSWER_VARIANTS_FOR_CURRENT_QUESTION, json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return request;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        JSONObject jsonObject = null;
        try {
            String json;
            JSONObject jo = new JSONObject(s);
            json = jo.getString("response");

            JsonParser parser = new JsonParser();
            JsonObject rootObejct = parser.parse(json).getAsJsonObject();
            JsonElement projectElement = rootObejct.get("competitions");

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            if (projectElement.isJsonObject()) {
                Competitions competitions = gson.fromJson(projectElement, Competitions.class);
                Globals.competitions = competitions;
            } else {
                Type type = new TypeToken<List<Competitions>>() {
                }.getType();
                List<Competitions> competitionsList = gson.fromJson(projectElement, type);
                if (competitionsList.size() != 0) {
                    Globals.competitions = competitionsList.get(0);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            Competitions com = Globals.competitions;
            if (!mIsCanceled) {
                inerface.processFinish();
            }
        }
    }
}
