package com.adtv.media.async;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.adtv.media.dto.Rating;
import com.adtv.media.other.Globals;
import com.adtv.media.other.URLConstants;
import com.adtv.media.other.UrlConnectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by kgb on 27.06.2015.
 */
public class RatingAsyncTask extends AsyncTask<Void, Void, String> {

    private RatingAsyncTaskInteface ratingAsyncTaskInteface = null;
    private boolean mIsCanceled;

    public RatingAsyncTask() {
    }

    public RatingAsyncTask(RatingAsyncTaskInteface ratingAsyncTaskInteface) {
        this.ratingAsyncTaskInteface = ratingAsyncTaskInteface;
        mIsCanceled = false;
    }

    public void cancelRequest() {
        mIsCanceled = true;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(Void... voids) {
        String request = "";
        if (Globals.userRegistered != null) {
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", Globals.userRegistered.getId());
                json.put("token", Globals.userRegistered.getToken());
                json.put("game_id", Globals.competitions.getGame_id());
                request = UrlConnectionUtils.postData(URLConstants.GET_RATING, json);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return request;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        JsonParser parser = new JsonParser();
        if (TextUtils.isEmpty(s)) {
            Globals.rating = new Rating();
            ratingAsyncTaskInteface.processFinish();
            return;
        }
        JsonObject rootObject = parser.parse(s).getAsJsonObject();
        JsonElement projectElement = rootObject.get("response");
        Gson gson = new GsonBuilder().create();

        if (projectElement.isJsonObject()) {
            Rating rating = gson.fromJson(projectElement, Rating.class);
            if (rating != null) {
                Globals.rating = rating;
            }
        } else {
            Type type = new TypeToken<List<Rating>>() {
            }.getType();
            List<Rating> ratingList = gson.fromJson(projectElement, type);
            if (ratingList.size() != 0) {
                Globals.rating = ratingList.get(0);
            } else {
                Globals.rating = new Rating();
            }
        }
        if (!mIsCanceled) {
            ratingAsyncTaskInteface.processFinish();
        }
    }
}
