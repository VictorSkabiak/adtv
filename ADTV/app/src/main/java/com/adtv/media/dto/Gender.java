package com.adtv.media.dto;

/**
 * Created by kgb on 17.06.2015.
 */
public class Gender {
    private Integer id;
    private String name;

    public Gender() {
    }

    public Gender(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
