package com.adtv.media.dto;

/**
 * Created by kgb on 17.06.2015.
 */
public class User {

    private String name;
    private String surname;
    private Integer gender;
    private Integer address_id;
    private String date_of_birth;
    private String email;
    private String phone;

    public User() {
    }

    public User(String name, String surname, Integer gender, Integer address_id, String date_of_birth, String email, String phone) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.address_id = address_id;
        this.date_of_birth = date_of_birth;
        this.email = email;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getAddress_id() {
        return address_id;
    }

    public void setAddress_id(Integer address_id) {
        this.address_id = address_id;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
